package com.cnsugar.common.mongodb;

import com.mongodb.BasicDBObject;

/**
 * @Description 更新对象
 * 
 * @Author Sugar
 * @Version 2017年1月10日 下午6:19:15
 * @Copyright cdsugar.com
 */
public class Update {
	private BasicDBObject incObjects = new BasicDBObject();
	private BasicDBObject setObjects = new BasicDBObject();
	private BasicDBObject unsetObjects = new BasicDBObject();

	public Update() {
	}

	public Update(BasicDBObject set) {
		this.setObjects = set;
	}

	public Update set(String key, Object value) {
		setObjects.append(key, value);
		return this;
	}
	
	public Update unset(String key) {
		unsetObjects.append(key, 0);
		return this;
	}

	public Update inc(String key, int value) {
		incObjects.append(key, value);
		return this;
	}

	public BasicDBObject set() {
		return setObjects;
	}
	
	public BasicDBObject unset() {
		return unsetObjects;
	}

	public BasicDBObject inc() {
		return incObjects;
	}

	public void reset() {
		setObjects.clear();
		incObjects.clear();
		unsetObjects.clear();
	}
}
