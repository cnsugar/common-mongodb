package com.cnsugar.common.mongodb;

import java.util.List;

import com.cnsugar.common.context.AppContext;
import com.cnsugar.common.paging.Page;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;

/**
 * @Description mongodb工具类
 * @Author Sugar
 * @Version 2017年1月10日 上午11:24:53
 * @Copyright cnsugar@163.com
 */
public class MongoUtils {
    private static MongoSupport mongoSupport;

    public final static MongoSupport getMongoSupport() {
        if (mongoSupport == null) {
            mongoSupport = AppContext.getBean(MongoSupport.class);
        }
        return mongoSupport;
    }

    /**
     * 获取所有集合名称
     *
     * @return List<String>
     */
    public final static List<String> getCollectionNames() {
        return getMongoSupport().getCollectionNames();
    }

    /**
     * 获取指定集合
     *
     * @param collectionName
     * @return
     */
    public final static MongoCollection<Document> getCollection(String collectionName) {
        return getMongoSupport().getCollection(collectionName);
    }

    /**
     * 查询所有数据, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @return List<Document>
     */
    public final static List<Document> findAll(String collectionName) {
        return getMongoSupport().find(collectionName, null, null, 0, 0);
    }

    /**
     * 查询所有数据, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param orderBy        排序条件，如：new BasicDBObject("key", OrderBy.ASC)
     * @return List<Document>
     */
    public final static List<Document> findAll(String collectionName, Bson orderBy) {
        return getMongoSupport().find(collectionName, null, orderBy, 0, 0);
    }

    /**
     * 查询指定数量数据, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param orderBy        排序条件，如：new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @return List<Document>
     */
    public final static List<Document> findAll(String collectionName, Bson orderBy, int limit) {
        return getMongoSupport().find(collectionName, null, orderBy, limit, 0);
    }

    /**
     * 查询指定数量数据, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param limit          查询记录数(0-所有)
     * @return List<Document>
     */
    public final static List<Document> findAll(String collectionName, int limit) {
        return getMongoSupport().find(collectionName, null, null, limit, 0);
    }

    /**
     * 查询指定数量数据, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<Document>
     */
    public final static List<Document> findAll(String collectionName, int limit, int skip) {
        return getMongoSupport().find(collectionName, null, null, limit, skip);
    }

    /**
     * 查询所有数据列表, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<Document>
     */
    public final static List<Document> findAll(String collectionName, Bson orderBy, int limit, int skip) {
        return getMongoSupport().find(collectionName, null, orderBy, limit, skip);
    }

    /**
     * 查询所有数据列表，返回指定实体类列表
     *
     * @param clazz 实体类，集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @return
     */
    public final static <T> List<T> findAll(Class<T> clazz) {
        return getMongoSupport().findAll(clazz);
    }

    /**
     * 查询所有数据列表，返回指定实体类列表
     *
     * @param clazz          实体类
     * @param collectionName 集合名
     * @return
     */
    public final static <T> List<T> findAll(Class<T> clazz, String collectionName) {
        return getMongoSupport().find(clazz, collectionName, null, null, 0, 0);
    }

    /**
     * 查询所有数据列表，返回指定实体类列表
     *
     * @param clazz          实体类
     * @param collectionName 集合名
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @return
     */
    public final static <T> List<T> findAll(Class<T> clazz, String collectionName, Bson orderBy) {
        return getMongoSupport().find(clazz, collectionName, null, orderBy, 0, 0);
    }

    /**
     * 查询所有数据列表，返回指定实体类列表
     *
     * @param clazz          实体类
     * @param collectionName 集合名
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @return
     */
    public final static <T> List<T> findAll(Class<T> clazz, String collectionName, Bson orderBy, int limit) {
        return getMongoSupport().find(clazz, collectionName, null, orderBy, limit, 0);
    }

    /**
     * 查询所有数据列表，返回指定实体类列表
     *
     * @param clazz          实体类
     * @param collectionName 集合名
     * @param limit          查询记录数(0-所有)
     * @return
     */
    public final static <T> List<T> findAll(Class<T> clazz, String collectionName, int limit) {
        return getMongoSupport().find(clazz, collectionName, null, null, limit, 0);
    }

    /**
     * 查询所有数据列表，返回指定实体类列表
     *
     * @param clazz          实体类
     * @param collectionName 集合名
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return
     */
    public final static <T> List<T> findAll(Class<T> clazz, String collectionName, int limit, int skip) {
        return getMongoSupport().find(clazz, collectionName, null, null, limit, skip);
    }

    /**
     * 查询所有数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<T>
     */
    public final static <T> List<T> findAll(Class<T> clazz, String collectionName, Bson orderBy, int limit, int skip) {
        return getMongoSupport().find(clazz, collectionName, null, orderBy, limit, skip);
    }

    /**
     * 根据条件查询数据列表
     *
     * @param collectionName 集合名称
     * @param filter         查询条件，如：Filters.eq("key","value")
     * @return List<Document>
     */
    public final static List<Document> find(String collectionName, Bson filter) {
        return getMongoSupport().find(collectionName, filter, null, 0, 0);
    }

    /**
     * 根据条件查询数据列表
     *
     * @param collectionName 集合名称
     * @param filter         查询条件，如：Filters.eq("key","value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @return List<Document>
     */
    public final static List<Document> find(String collectionName, Bson filter, Bson orderBy) {
        return getMongoSupport().find(collectionName, filter, orderBy, 0, 0);
    }

    /**
     * 根据条件查询数据列表
     *
     * @param collectionName 集合名称
     * @param filter         查询条件，如：Filters.eq("key","value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @return List<Document>
     */
    public final static List<Document> find(String collectionName, Bson filter, Bson orderBy, int limit) {
        return getMongoSupport().find(collectionName, filter, orderBy, 0, 0);
    }

    /**
     * 根据条件查询数据列表
     *
     * @param collectionName 集合名称
     * @param filter         查询条件，如：Filters.eq("key","value")
     * @param limit          查询记录数(0-所有)
     * @return List<Document>
     */
    public final static List<Document> find(String collectionName, Bson filter, int limit) {
        return getMongoSupport().find(collectionName, filter, null, 0, 0);
    }

    /**
     * 根据条件查询数据列表
     *
     * @param collectionName 集合名称
     * @param filter         查询条件，如：Filters.eq("key","value")
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<Document>
     */
    public final static List<Document> find(String collectionName, Bson filter, int limit, int skip) {
        return getMongoSupport().find(collectionName, filter, null, 0, 0);
    }

    /**
     * 查询数据列表, 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<Document>
     */
    public final static List<Document> find(String collectionName, Bson filter, Bson orderBy, int limit, int skip) {
        return getMongoSupport().find(collectionName, filter, orderBy, limit, skip);
    }

    /**
     * 查询数据列表(去重), 返回Map结果集
     *
     * @param collectionName 集合名称
     * @param fieldName      要去重的字段
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return List<Map<String, Object>>
     */
    public List<Document> distinct(String collectionName, String fieldName, Bson filter) {
        return getMongoSupport().distinct(collectionName, fieldName, filter);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz  Java实体对象, 集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter 查询条件, 如: Filters.eq("key", "value")
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, Bson filter) {
        return getMongoSupport().find(clazz, filter);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz   Java实体对象, 集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter  查询条件, 如: Filters.eq("key", "value")
     * @param orderBy 排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, Bson filter, Bson orderBy) {
        return getMongoSupport().find(clazz, filter, orderBy);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz   Java实体对象, 集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter  查询条件, 如: Filters.eq("key", "value")
     * @param orderBy 排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit   查询记录数(0-所有)
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, Bson filter, Bson orderBy, int limit) {
        return getMongoSupport().find(clazz, filter, orderBy, limit);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz  Java实体对象, 集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter 查询条件, 如: Filters.eq("key", "value")
     * @param limit  查询记录数(0-所有)
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, Bson filter, int limit) {
        return getMongoSupport().find(clazz, filter, limit);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz  Java实体对象, 集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter 查询条件, 如: Filters.eq("key", "value")
     * @param limit  查询记录数(0-所有)
     * @param skip   跳过记录数
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, Bson filter, int limit, int skip) {
        return getMongoSupport().find(clazz, filter, limit, skip);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, String collectionName, Bson filter) {
        return getMongoSupport().find(clazz, collectionName, filter, null, 0, 0);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, String collectionName, Bson filter, Bson orderBy) {
        return getMongoSupport().find(clazz, collectionName, filter, orderBy, 0, 0);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, String collectionName, Bson filter, Bson orderBy, int limit) {
        return getMongoSupport().find(clazz, collectionName, filter, orderBy, limit, 0);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param limit          查询记录数(0-所有)
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, String collectionName, Bson filter, int limit) {
        return getMongoSupport().find(clazz, collectionName, filter, null, limit, 0);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, String collectionName, Bson filter, int limit, int skip) {
        return getMongoSupport().find(clazz, collectionName, filter, null, limit, skip);
    }

    /**
     * 查询数据列表, 返回Java实体对象结果集
     *
     * @param clazz          Java实体对象, 如: User.class
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @param limit          查询记录数(0-所有)
     * @param skip           跳过记录数
     * @return List<T>
     */
    public final static <T> List<T> find(Class<T> clazz, String collectionName, Bson filter, Bson orderBy, int limit, int skip) {
        return getMongoSupport().find(clazz, collectionName, filter, orderBy, limit, skip);
    }

    /**
     * 查询单条记录, 返回Map对象
     *
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return Document
     */
    public final static Document findOne(String collectionName, Bson filter) {
        return getMongoSupport().findOne(collectionName, filter);
    }

    /**
     * 查询单条记录, 返回Java实体对象
     *
     * @param clazz  Java实体对象, 集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter 查询条件, 如: Filters.eq("key", "value")
     * @return <T>
     */
    public final static <T> T findOne(Class<T> clazz, Bson filter) {
        return getMongoSupport().findOne(clazz, filter);
    }

    /**
     * 查询单条记录, 返回Java实体对象
     *
     * @param clazz          Java实体对象
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return <T>
     */
    public final static <T> T findOne(Class<T> clazz, String collectionName, Bson filter) {
        return getMongoSupport().findOne(clazz, collectionName, filter);
    }

    /**
     * 分页查询
     *
     * @param page 分页设置对象，必须设置clazz属性，集合名与clazz对象类名不相同时需要配置注解@Collection(
     *             "collectionName")
     * @return Page
     */
    public final static Page findPage(Page page) {
        return getMongoSupport().findPage(page);
    }

    /**
     * 分页查询
     *
     * @param page           分页设置对象
     * @param collectionName 集合名称
     * @return Page
     */
    public final static Page findPage(Page page, String collectionName) {
        return getMongoSupport().findPage(page, collectionName, null, null);
    }

    /**
     * 分页查询
     *
     * @param page   分页设置对象，必须设置clazz属性，集合名与clazz对象类名不相同时需要配置注解@Collection(
     *               "collectionName")
     * @param filter 查询条件, 如: Filters.eq("key", "value")
     * @return Page
     */
    public final static Page findPage(Page page, Bson filter) {
        return getMongoSupport().findPage(page, filter);
    }

    /**
     * 分页查询
     *
     * @param page           分页设置对象
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return Page
     */
    public final static Page findPage(Page page, String collectionName, Bson filter) {
        return getMongoSupport().findPage(page, collectionName, filter, null);
    }

    /**
     * 分页查询
     *
     * @param page    分页设置对象，必须设置clazz属性，集合名与clazz对象类名不相同时需要配置注解@Collection(
     *                "collectionName")
     * @param filter  查询条件, 如: Filters.eq("key", "value")
     * @param orderBy 排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @return Page
     */
    public final static Page findPage(Page page, Bson filter, Bson orderBy) {
        return getMongoSupport().findPage(page, filter, orderBy);
    }

    /**
     * 分页查询
     *
     * @param page           分页设置对象
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param orderBy        排序, 如: new BasicDBObject("key", OrderBy.ASC)
     * @return Page
     */
    public final static Page findPage(Page page, String collectionName, Bson filter, Bson orderBy) {
        return getMongoSupport().findPage(page, collectionName, filter, orderBy);
    }

    /**
     * 通过_id查询记录
     *
     * @param clazz 实体类，集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param id
     * @return <T>
     */
    public final static <T> T findById(Class<T> clazz, String id) {
        return getMongoSupport().findById(clazz, id);
    }

    /**
     * 通过_id查询记录
     *
     * @param clazz          实体类
     * @param collectionName 集合名称
     * @param id
     * @return <T>
     */
    public final static <T> T findById(Class<T> clazz, String collectionName, String id) {
        return getMongoSupport().findById(clazz, collectionName, id);
    }

    /**
     * 通过_id查询记录，返回Map
     *
     * @param collectionName 集合名称
     * @param id
     * @return Document
     */
    public final static Document findById(String collectionName, String id) {
        return getMongoSupport().findById(collectionName, id);
    }

    /**
     * 查询集合记录数
     *
     * @param clazz 实体类，集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @return long
     */
    public final static <T> long count(Class<T> clazz) {
        return getMongoSupport().count(clazz);
    }

    /**
     * 查询集合记录数
     *
     * @param collectionName 集合名称
     * @return long
     */
    public final static long count(String collectionName) {
        return getMongoSupport().count(collectionName);
    }

    /**
     * 查询集合记录数
     *
     * @param clazz  实体类，集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param filter 查询条件, 如: Filters.eq("key", "value")
     * @return long
     */
    public final static <T> long count(Class<T> clazz, Bson filter) {
        return getMongoSupport().count(clazz, filter);
    }

    /**
     * 查询集合记录数
     *
     * @param collectionName 集合名称
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return long
     */
    public final static long count(String collectionName, Bson filter) {
        return getMongoSupport().count(collectionName, filter);
    }

    /**
     * 分组统计记录数
     *
     * @param collectionName 集合名称
     * @param groupBy        分组字段
     * @return
     */
    public final static List<Document> count(String collectionName, String[] groupBy) {
        return getMongoSupport().count(collectionName, groupBy, null, 0);
    }

    /**
     * 分组统计记录数
     *
     * @param collectionName 集合名称
     * @param groupBy        分组字段
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @return
     */
    public final static List<Document> count(String collectionName, String[] groupBy, Bson filter) {
        return getMongoSupport().count(collectionName, groupBy, filter, 0);
    }

    /**
     * 分组统计记录数
     *
     * @param collectionName 集合名称
     * @param groupBy        分组字段
     * @param filter         查询条件, 如: Filters.eq("key", "value")
     * @param limit          数据集合大小限制
     * @return
     */
    public final static List<Document> count(String collectionName, String[] groupBy, Bson filter, int limit) {
        return getMongoSupport().count(collectionName, groupBy, filter, limit);
    }

    /**
     * 使用mapReduce进行统计
     *
     * @param collectionName
     * @param mapFunction
     * @param reduceFunction
     * @return
     */
    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, null, null, null, 0);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, Bson filter) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, null, filter, null, 0);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, Bson filter, Bson orderBy) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, null, filter, orderBy, 0);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, Bson filter, int limit) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, null, filter, null, limit);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, Bson filter, Bson orderBy, int limit) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, null, filter, orderBy, limit);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, String finalizeFunction) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, finalizeFunction, null, null, 0);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, String finalizeFunction, Bson filter) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, finalizeFunction, filter, null, 0);
    }

    public final static List<Document> mapReduce(String collectionName, String mapFunction, String reduceFunction, String finalizeFunction, Bson filter, int
            limit) {
        return getMongoSupport().mapReduce(Document.class, collectionName, mapFunction, reduceFunction, finalizeFunction, filter, null, limit);
    }

    /**
     * 保存对象
     *
     * @param entity Map或实体类对象，集合名与类名不相同时需要配置注解@Collection("collectionName")
     */
    public final static void save(Object entity) {
        getMongoSupport().save(entity);
    }

    /**
     * 保存对象
     *
     * @param entity         Map或实体类对象
     * @param collectionName 集合名称
     */
    public final static void save(Object entity, String collectionName) {
        getMongoSupport().save(entity, collectionName);
    }

    /**
     * 批量保存对象
     *
     * @param list Map或实体类对象列表，集合名与类名不相同时需要配置注解@Collection("collectionName")
     */
    public final static void saveAll(List<?> list) {
        getMongoSupport().saveAll(list);
    }

    /**
     * 批量保存对象
     *
     * @param list           Map或实体类对象列表
     * @param collectionName 集合名称
     */
    public final static void saveAll(List<?> list, String collectionName) {
        getMongoSupport().saveAll(list, collectionName);
    }

    /**
     * 更新记录
     *
     * @param entity 实体类对象列表，必须设置_id属性的值，集合名与类名不相同时需要配置注解@Collection(
     *               "collectionName")
     * @return boolean
     */
    public final static <T> boolean update(T entity) {
        return getMongoSupport().update(entity);
    }

    /**
     * 更新记录
     *
     * @param entity         实体类对象列表，必须设置_id属性的值
     * @param collectionName 集合名称
     * @return boolean
     */
    public final static <T> boolean update(T entity, String collectionName) {
        return getMongoSupport().update(entity, collectionName);
    }

    /**
     * 更新单条记录(UpdateOne)
     *
     * @param entity 实体类对象
     * @param filter 更新条件, 如: Filters.eq("key", "value")
     * @return long 更新记录数
     */
    public final static <T> long update(T entity, Bson filter) {
        return getMongoSupport().update(entity, filter, false);
    }

    /**
     * 更新记录
     *
     * @param entity 实体类对象
     * @param filter 更新条件, 如: Filters.eq("key", "value")
     * @param mutil  true-更新所有，false-更新一条
     * @return long 更新记录数
     */
    public final static <T> long update(T entity, Bson filter, boolean mutil) {
        return getMongoSupport().update(entity, filter, mutil);
    }

    /**
     * 更新单条记录(UpdateOne)
     *
     * @param collectionName 集合名称
     * @param filter         更新条件, 如: Filters.eq("key", "value")
     * @param update         更新数据
     * @return long 更新记录数
     */
    public final static long update(String collectionName, Bson filter, Update update) {
        return getMongoSupport().update(collectionName, filter, update, false);
    }

    /**
     * 更新记录
     *
     * @param collectionName 集合名称
     * @param filter         更新条件, 如: Filters.eq("key", "value")
     * @param update         更新数据
     * @param mutil          true-更新所有，false-更新一条
     * @return long 更新记录数
     */
    public final static long update(String collectionName, Bson filter, Update update, boolean mutil) {
        return getMongoSupport().update(collectionName, filter, update, mutil);
    }

    /**
     * 通过_id更新记录
     *
     * @param entity 实体类对象，集合名与类名不相同时需要配置注解@Collection("collectionName")
     * @param id
     * @return boolean
     */
    public final static <T> boolean updateById(T entity, String id) {
        return getMongoSupport().updateById(entity, id);
    }

    /**
     * 通过_id更新记录
     *
     * @param entity         实体类对象
     * @param collectionName 集合名称
     * @param id
     * @return boolean
     */
    public final static <T> boolean updateById(T entity, String collectionName, String id) {
        return getMongoSupport().updateById(entity, collectionName, id);
    }

    /**
     * 通过_id更新记录
     *
     * @param collectionName 集合名称
     * @param update         更新数据
     * @param id
     * @return boolean
     */
    public final static boolean updateById(String collectionName, Update update, String id) {
        return getMongoSupport().updateById(collectionName, update, id);
    }

    /**
     * 通过_id更新记录
     *
     * @param collectionName 集合名称
     * @param update         更新数据(Bson对象)
     * @param id
     * @return boolean
     */
    public final static boolean updateById(String collectionName, Bson update, String id) {
        return getMongoSupport().updateById(collectionName, update, id);
    }

    /**
     * 删除一条记录
     *
     * @param collectionName 集合名称
     * @param filter         删除条件，如: Filters.eq("key", "value")
     * @return boolean
     */
    public final static boolean deleteOne(String collectionName, Bson filter) {
        return getMongoSupport().deleteOne(collectionName, filter);
    }

    /**
     * 删除所有记录
     *
     * @param collectionName 集合名称
     * @param filter         删除条件，如: Filters.eq("key", "value")
     * @return boolean
     */
    public final static long deleteAll(String collectionName, Bson filter) {
        return getMongoSupport().deleteAll(collectionName, filter);
    }

    /**
     * 通过_id删除记录
     *
     * @param collectionName 集合名称
     * @param id
     * @return boolean
     */
    public final static boolean deleteById(String collectionName, String id) {
        return getMongoSupport().deleteById(collectionName, id);
    }

    /**
     * 删除集合
     *
     * @param collectionName 集合名称
     */
    public final static void drop(String collectionName) {
        getMongoSupport().drop(collectionName);
    }

    /**
     * 创建索引
     *
     * @param collectionName 集合名称
     * @param keys
     */
    public final static void createIndex(String collectionName, Bson keys) {
        getMongoSupport().createIndex(collectionName, keys);
    }

    /**
     * 删除所有索引
     *
     * @param collectionName 集合名称
     */
    public final static void dropIndexes(String collectionName) {
        getMongoSupport().dropIndexes(collectionName);
    }

    /**
     * 删除指定索引
     *
     * @param collectionName 集合名称
     * @param indexName      索引名称
     */
    public final static void dropIndex(String collectionName, String indexName) {
        getMongoSupport().dropIndex(collectionName, indexName);
    }
}