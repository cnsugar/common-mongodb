package com.cnsugar.common.mongodb;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @name Collection
 * @description	定义集合
 *
 * @author Sugar
 * @date 2016年1月20日 上午10:26:41
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Collection {
	String value() default "";
}
