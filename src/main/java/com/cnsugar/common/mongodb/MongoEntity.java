package com.cnsugar.common.mongodb;

import org.bson.types.ObjectId;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @Name com.utils.db.mongodb.MongoEntity
 * @Description mongodb实体
 * 
 * @Author Sugar
 * @Version 2017年1月10日 下午3:33:58
 * @Copyright cnsugar@163.com
 */
public class MongoEntity {
	@JSONField(name="_id", ordinal = 0, serialize = false)
	protected String id;
	
	public ObjectId getObjectId() {
		if (id == null) {
			return null;
		}
		return new ObjectId(id);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
