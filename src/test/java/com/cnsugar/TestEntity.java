package com.cnsugar;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.cnsugar.common.mongodb.Collection;
import com.cnsugar.common.mongodb.MongoEntity;

/**
 * 测试实体
 *
 * @Author Sugar
 * @Version 2016年12月14日 下午6:34:01
 */
@Collection("test")
public class TestEntity extends MongoEntity {
    @JSONField(ordinal = 2)
    private String name;

    @JSONField(ordinal = 3)
    private int status; // ０－待发送，１－未确认；２－发送成功；３－发送失败；

    //字段名与实体名不一致，需要增加name配置
    @JSONField(name = "test_score", ordinal = 4)
    private Float testScore;

    @JSONField(ordinal = 5)
    private Date onTime = new Date();

    @JSONField(name = "child", ordinal = 6)
    private List<Map<String, Object>> childList;

    //不需要保存到数据库中的字段，serialize设为false
    @JSONField(serialize = false)
    private String msgId;

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    public Float getTestScore() {
        return testScore;
    }

    public void setTestScore(Float testScore) {
        this.testScore = testScore;
    }

    public Date getOnTime() {
        return onTime;
    }

    public void setOnTime(Date onTime) {
        this.onTime = onTime;
    }

    /**
     * @return the childList
     */
    public List<Map<String, Object>> getChildList() {
        return childList;
    }

    /**
     * @param childList the childList to set
     */
    public void setChildList(List<Map<String, Object>> childList) {
        this.childList = childList;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", name:" + name +
                ", status:" + status +
                ", testScore:" + testScore +
                ", onTime:" + onTime +
                ", childList:" + childList +
                ", msgId:" + msgId +
                '}';
    }
}
