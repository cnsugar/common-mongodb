package com.cnsugar;

import java.util.*;

import com.cnsugar.common.paging.Page;
import com.cnsugar.common.mongodb.MongoUtils;
import com.cnsugar.common.mongodb.Update;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.model.Filters;
import org.bson.types.ObjectId;


/**
 * @author Sugar
 * @name Test
 * @description
 * @date 2016年2月22日 下午10:19:06
 */
public class Test {
    @org.junit.Test
    public void testSave() {
        //保存实体对象
        TestEntity entity = new TestEntity();
        entity.setName("sugar");
        entity.setStatus(2);
        entity.setTestScore(87f);
        MongoUtils.save(entity);

        //保存map对象，需要指定集合名称
        Map<String, Object> map = new HashMap<>();
        map.put("name", "zhangshan");
        map.put("status", 2);
        map.put("test_score", 82.5f);
        map.put("onTime", new Date().getTime());
        MongoUtils.save(map, "test");

        //保存Document对象，与map类似，需要指定集合名称
        Document doc = new Document();
        doc.put("name", "lishi");
        doc.put("status", 2);
        doc.put("test_score", 99f);
        doc.put("onTime", new Date().getTime());
        MongoUtils.save(doc, "test");
    }

    @org.junit.Test
    public void testUpdate() {
        //更新java对象，必须要指定id，如果字段值为null，不会更新旧的数据
        TestEntity entity = new TestEntity();
        entity.setId("5c343804fdfad4230852e1f5");
        entity.setName("sugar2");
        entity.setStatus(1);
        MongoUtils.update(entity);

        //自定义更新的集合名、条件、字段
        String collectionName = "test";
        Bson filter = Filters.eq("_id", new ObjectId("5c343804fdfad4230852e1f6"));
        Update update = new Update();
        update.set("name", "zhangshan2");
        update.inc("status", 1);//相当于status += 1
        MongoUtils.update(collectionName, filter, update);
    }

    @org.junit.Test
    public void testQuery() {
        //查询出实体列表
        List<TestEntity> ll = MongoUtils.findAll(TestEntity.class);
        System.out.println(ll);

        //查询Document对象列表，需要指定集合名
        List<Document> list = MongoUtils.findAll("test");
        System.out.println(list);

        //用Filters生成条件查询，查询名字以2结尾的数据
        List<TestEntity> ll2 = MongoUtils.find(TestEntity.class, Filters.regex("name", ".*2"));
        System.out.println(ll2);

        //分页查询，查询分数大于90的数据，查询第1页，每页10条
        Page page = new Page(10, 1);
        page.setClazz(TestEntity.class);//指定列表中的对象类型
        page = MongoUtils.findPage(page, Filters.gt("test_score", 90));
        System.out.println(page.getList());
    }

    @org.junit.Test
    public void testDelete() {
        //根据ID删除
        MongoUtils.deleteById("test", "587482defdfad41a9c94c9b6");

        //删除一条数据
        MongoUtils.deleteOne("test", Filters.eq("_id", new ObjectId("587482defdfad41a9c94c9b6")));

        //批量删除
        List<ObjectId> del = new ArrayList<ObjectId>();
        del.add(new ObjectId("587482defdfad41a9c94c9b6"));
        del.add(new ObjectId("58748350fdfad41a1c5fba14"));
        del.add(new ObjectId("5874930ffdfad40df031215a"));
        MongoUtils.deleteAll("test", Filters.in("_id", del));
    }

    @org.junit.Test
    public void testCount() {
        //统计test表数据总数
        long count = MongoUtils.count("test");

        //统计test表中status=2的数据总数
        long count2 = MongoUtils.count("test", Filters.eq("status", 2));

        //根据status进行分组统计
        List<Document> list = MongoUtils.count("test", new String[]{"status"});
        System.out.println(list);

        //自定义mapReduce函数进行数据分析，按天统计数据总数和status=1的总数
        StringBuilder mapFunction = new StringBuilder("function(){emit(");
        mapFunction.append("new Date(this.onTime).toLocaleDateString()");
        mapFunction.append(",{count:1, send:this.status==1?1:0}");
        mapFunction.append(");}");
        StringBuilder reduceFunction = new StringBuilder("function(key, values){");
        reduceFunction.append("var _total = 0, _send = 0;");
        reduceFunction.append("values.forEach(function(val){_total += val.count; _send += val.send;});");
        reduceFunction.append("return {count:_total, send:_send};");
        reduceFunction.append("}");
        List<Document> list2 = MongoUtils.mapReduce("test", mapFunction.toString(), reduceFunction.toString());
        System.out.println(list2);
    }
}
